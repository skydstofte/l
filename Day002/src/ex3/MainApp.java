package ex3;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.shape.Polygon;


public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
        Circle circle = new Circle(240, 100, 30);
        pane.getChildren().add(circle);
        circle.setFill(Color.YELLOW);
        circle.setStroke(Color.YELLOW);
        
        Rectangle wall = new Rectangle(50, 200, 120, 120);
        pane.getChildren().add(wall);
        wall.setFill(Color.RED);
        
        Rectangle window = new Rectangle(70, 220, 40, 40);
        pane.getChildren().add(window);
        
        Polygon polygon = new Polygon(30, 200, 110, 140, 190, 200);
        pane.getChildren().add(polygon);
        polygon.setFill(Color.GREEN);
        
    }

}
