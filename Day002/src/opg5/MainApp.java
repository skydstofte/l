package opg5;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
        Circle c1 = new Circle(70, 70, 50);
        pane.getChildren().add(c1);
        c1.setFill(null);
        c1.setStroke(Color.BLUE);
        c1.setStrokeWidth(3);
        
        Circle c2 = new Circle(170, 70, 50);
        pane.getChildren().add(c2);
        c2.setFill(null);
        c2.setStroke(Color.BLACK);
        c2.setStrokeWidth(3);
        
        Circle c3 = new Circle(270, 70, 50);
        pane.getChildren().add(c3);
        c3.setFill(null);
        c3.setStroke(Color.RED);
        c3.setStrokeWidth(3);
        
        Circle c4 = new Circle(120, 120, 50);
        pane.getChildren().add(c4);
        c4.setFill(null);
        c4.setStroke(Color.YELLOW);
        c4.setStrokeWidth(3);
        
        Circle c5 = new Circle(220, 120, 50);
        pane.getChildren().add(c5);
        c5.setFill(null);
        c5.setStroke(Color.GREEN);
        c5.setStrokeWidth(3);
    }

}
