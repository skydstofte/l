package opg4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
    	
        Circle c1 = new Circle(200, 200, 100);
        pane.getChildren().add(c1);
        c1.setFill(Color.BLACK);
        c1.setStroke(Color.BLACK);

        Circle c2 = new Circle(200, 200, 80);
        pane.getChildren().add(c2);
        c2.setFill(Color.WHITE);

        Circle c3 = new Circle(200, 200, 60);
        pane.getChildren().add(c3);
        c3.setFill(Color.BLACK);
        c3.setStroke(Color.BLACK);

        Circle c4 = new Circle(200, 200, 40);
        pane.getChildren().add(c4);
        c4.setFill(Color.WHITE);
        c4.setStroke(Color.BLACK);
        
        Circle c5 = new Circle(200, 200, 20);
        pane.getChildren().add(c5);
        c5.setFill(Color.BLACK);
        c5.setStroke(Color.BLACK);
    }

}
