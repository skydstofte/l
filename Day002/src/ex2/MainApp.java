package ex2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
    	int coordinateRight = 100;
        Circle circle = new Circle(70 + coordinateRight, 70, 30);
        pane.getChildren().add(circle);
        circle.setFill(Color.CORNFLOWERBLUE);
        circle.setStroke(Color.BLACK);
        
        Rectangle rectangle = new Rectangle(200 + coordinateRight, 200, 50, 100);
        pane.getChildren().add(rectangle);
        rectangle.setFill(Color.INDIANRED);
        rectangle.setStroke(Color.BLUEVIOLET);
        
        Line line = new Line(50 + coordinateRight, 100, 60 + coordinateRight, 300);
        pane.getChildren().add(line);
        line.setFill(Color.AZURE);
        line.setStroke(Color.AQUA);
        
    }

}
