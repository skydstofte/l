package opg3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
    	Circle circleBig = new Circle(200, 200, 100);
    	pane.getChildren().add(circleBig);
    	circleBig.setFill(null);
    	circleBig.setStroke(Color.BLACK);
    	
    	Circle eye1 = new Circle(130, 200, 15);
    	pane.getChildren().add(eye1);
    	eye1.setFill(null);
    	eye1.setStroke(Color.BLACK);
    	
    	Circle eye2 = new Circle(190, 200, 15);
    	pane.getChildren().add(eye2);
    	eye2.setFill(null);
    	eye2.setStroke(Color.BLACK);
    	
    	Line mouth = new Line(135, 240, 185, 240);
    	pane.getChildren().add(mouth);
    	mouth.setFill(Color.BLACK);
    	
    	
    	
    	
        
    }

}
